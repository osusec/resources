# Connecting to an OSUSEC `WireGuard` VPN

## Debian-Based Linux

1. Generate a private and public key pair for use with WireGuard:

```sh
wg genkey | tee privatekey | wg pubkey > publickey
```

2. Send your public key to the OSUSEC Discord Bot in the `#botspam` channel:

```
!vpn <public key>
```

3. Copy the configuration that the Discord Bot returns into the file `/etc/wireguard/wg0.conf`. Replace the placeholder with your private key generated in Step 1. An example configuration is shown below:

```ini
[Interface]
PrivateKey = <your private key here>
ListenPort = 1000
Address = 10.0.0.1/32
DNS = 1.1.1.1

[Peer]
PublicKey = <server public key>
AllowedIPs = 10.0.0.0/24, 10.1.0.0/24, 10.2.0.0/24
Endpoint = 69.69.69.69:1000
```

4. Start up WireGuard's `wg0` VPN connection:

```sh
sudo wg-quick up wg0
```

5. Use `ping` to check that you are connected to the network, by pinging a machine that is on the VPN. 

### Disconnecting

1. Run the following command to turn off the VPN connection:

```sh
sudo wg-quick down wg0
```

### Debugging

- If you encounter an error related to `resolvconf`, such as ` resolvconf: command not found`, try installing the `openresolv` package to fix this issue.
- If you cannot connect to internet with the VPN on, try pinging a DNS server like `1.1.1.1`, if the ping succeeds, use `1.1.1.1` as your DNS Server for wireguard (provided that it does not clobber any existing, necessary DNS entry). To do this, change the `DNS = x.x.x.x` line in the config file `/etc/wireguard/wg0.conf`.
- For further debugging, try `wg show` to see the specifics of your connection, and correlate this with `wg show` on the server.

## Windows

1. Open WireGuard and press `ctrl + N` to create a new empty tunnel. You should see a public key and private key generated for you.
   
2. Send your public key to the OSUSEC Discord Bot in the `#botspam` channel:
   
```
!vpn <public key>
```

3. Copy the configuration that the Discord Bot returns into the editable area of your empty tunnel. Replace the placeholder with your private key generated in Step 1. An example configuration is shown below:

```ini
[Interface]
PrivateKey = <your private key here>
ListenPort = 1000
Address = 10.0.0.1/32
DNS = 1.1.1.1

[Peer]
PublicKey = <server public key>
AllowedIPs = 10.0.0.0/24, 10.1.0.0/24, 10.2.0.0/24
Endpoint = 69.69.69.69:1000
```

4. Give your tunnel a name, then click `Save`. You can start up your VPN connection by simply clicking the `Activate` button.
5. Use `ping` to check that you are connected to the network, by pinging a machine that is on the VPN. 

### Debugging

- Using Wirguard in conjunction with WSL on Windows can sometimes lead to undefined behavior. If you experience any issues with being unable to ping or SSH a machine, try doing it through either powershell or standard command prompt (both of which come built in with SSH capabilities).