# Connecting to an OpenVPN server

OVPNs are often used for connections to CDC environments.

## Debian-Based Linux

1. OpenVPN is in the Apt repository:

```
sudo apt update
sudo apt install openvpn
```

2. To connect to the OVPN connection, run:

```
sudo openvpn --config [filename.ovpn]
```

You should see output that ends in something like the following line:

```
Fri Mar 11 14:01:59 2022 Initialization Sequence Completed
```

If you do not see "Initialization Sequence Completed", something has likely gone wrong. From here, you should be able to access what you need to that is behind VPN.

## Windows

1. Download the Community version of the OpenVPN client

[https://openvpn.net/community-downloads/](https://openvpn.net/community-downloads/)

Run the MSI and it should be installed easily. When running, the OpenVPN program is accessible from the Taskbar in the bottom right corner.

2. To connect to the OVPN connection:

Right click the Taskbar icon and go to "Import file..." Navigate to and choose the file. It's now imported in OpenVPN. 

![](./openvpn_images/win_import.png)

Right click the OpenVPN icon again, and go to "Connect." If you have several OVPNs imported, it will give you a choice between them. 

![](./openvpn_images/win_connect.png)

There will be a Windows notification and the Taskbar icon will change as a result.

![](./openvpn_images/win_notif.png)

![](./openvpn_images/win_green.png)

# Verifying the connection

A good way to do this is to try and access a box that you know you should be able to access.

If you're trying to verify an OVPN.com connection, you can do so with the following methods:

## Both Windows and Linux

You can hard-refresh the ovpn.com website and see the following:

![](./openvpn_images/ovpn_conn.png)

Additionally, if you go to Google, it should give indicators for the country that your OpenVPN connection is going through.

And "whats my IP" is always a reliable option.

## Linux 

You can also test by running the following command on Linux:

```
curl https://www.ovpn.com/v2/api/client/ptr
```

You should recieve the following output:

```
{"status":true,"ip":"the external ip","ptr":"PTR for the IP address"}
```