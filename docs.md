# Lab Manager Documentation

## Connecting to ESXI and iDRAC
Make sure you have access to `fw-access.eecs.oregonstate.edu`. If not, you will need to contact leanne@engr.oregonstate.edu in order to be added.  

Once you know you have access, put this in your `~/.ssh/config`, replacing XXX with your ONID:

```
Host osu
  HostName access.engr.oregonstate.edu
  User XXX
  ForwardAgent yes

Host fw-access
  HostName fw-access.eecs.oregonstate.edu
  User XXX
  ProxyJump osu
```

To connect to ESXI, run `$ ssh -L 8080:club-security-vmhost.eecs.oregonstate.edu:443 fw-access`.  
To connect to iDRAC, run `$ ssh -L 8080:club-security-mgt.eecs.oregonstate.edu:443 fw-access`.  

Navigate to `https://localhost:8080` to access appropriate login. Credentials are located in our password manager.

## OSUSEC Active Directory

OSUSEC has an Active Directory setup for our on-prem infrastructure:

| DNS                 | IP           | OS      | Description               |
|---------------------|--------------|---------|---------------------------|
| mewtwo.sinnoh.ctf   | 10.100.1.45  | Windows | Domain Controller and DNS |
| blazekin.sinnoh.ctf | 10.100.1.42  | Linux   | CTF Workstation           |
| arceus.sinnoh.ctf   | 100.21.91.50 | Linux   | VPN                       |
| dialga.sinnoh.ctf   | 10.100.1.44  | Windows | IDA Box                   |
| snorlax.sinnoh.ctf  | 10.100.1.46  | Linux   | SMB                       |

## Adding Sudo Users to Remote Machines

1. SSH into your machine and SU to `root`.  
2. Run `$ adduser newuser`, where `newuser` is the username of the user you want to add.  
3. Run `$ usermod -aG sudo newuser`.
4. Switch to the new user account with `$ su - newuser`.
5. Create `.ssh` folder in home directory: `$ mkdir ~/.ssh`.
6. Create `authorized_keys` file: `$ vim ~/.ssh/authorized_keys`.
7. Paste new user's SSH public key here, save and close file.

## Spinning Up CTF Challenges on Remote Server
The current setup for challenge hosting is messy and should be redone in the near future, as it breaks all the time. With that being said, the current process for adding a remote challenge is as follows:

1. Use `scp` to copy the challenge to the remote hosting machine. `$ scp challenge.zip remote_username@remote_ip:/remote/directory`
2. SSH into host machine, unzip challenge and navigate to challenge directory.
3. If using a `docker-compose.yml`, then to start the application run `$ sudo docker-compose up`. If using a `Docker` file, run `$ sudo docker build . -t name_of_chal` and wait for the image to build. Once completed, run `$ sudo docker run -d -p PORT:PORT -i name_of_chal`, with `PORT` being the port number specified in the challenge configs.
4. Run `$ sudo docker ps` to ensure service started correctly.
5. Check for any "side products" that can be deleted by running `$ sudo docker image ls`. If you see any repositories with the name `<none>`, delete them by running `$ sudo docker image rm IMAGE_ID`.
6. Be sure to test that your service is working properly and that the exploit script works remotely.

## Deploying a Machine in AWS
To access our AWS page, navigate to `https://login.oregonstate.edu/apps/aws/` and sign in.  There are two primary regions that we use: `us-west-1` and `us-west-2`. The region `us-west-1` is dedicated solely to providing cloud infrastructure for Cyber Defense Competition (CDC) practice. The region `us-west-2` contains everything else. To start to deploy a machine, navigate to the appropriate region's EC2 page and click `Launch Instance`.  

Select the AMI that you wish to deploy, you will be redirected to choose an Instance Type. It is recommended that you stick with `t3a.XXX` machines, and keep the size as small as possible to accomplish what you need. For example, if you need to spin up a machine for hosting one CTF challenge, then something as small as a `t3a.nano` will get the job done. Click `Next: Configure Instance Details` to continue.  

If you are deploying a CDC machine in `us-west-1`, you will want to make sure the machine is behind the CDC VPN. To do this, click on the drop down next to `Network` and select `vpc-0a7e86e70f2839756 | CDC-VPN`. Then scroll down to the section `Network Intefaces`, and under the `eth0` device, type in a primary IP within the subnet `10.1.20.0/24` that isn't already in use. If you are not deploying a CDC machine, then you can just move on to the next pages.

Keep cicking next until you get to the `Configure Security Group` page. Here, add the rule `All Traffic` and give it the source `Anywhere`.  

Select `Review and Launch`, and then `Launch`. You will be prompted for a key pair. Once you launch your instance, it will take several minutes for it to become accessible.  

## Route 53

Sometimes you want to be able to access an AWS box using a domain name rather than an IP address. Route 53 is scalable Domain Name System service that allows you to do just that. 

1. Go to the Route 53 service home page
2. Select `Hosted Zones` and select `osusec.org`.
3. Type in the record name, select your record type, and then specify the public IP of the box you are routing in the values field.
4. Create the record.

Note that if the IP specified is not an Elastic IP then there is a chance that the IP given will no longer point to your domain name if the machine is shut off. For this reason, only use Route 53 for machines with an Elastic IP or are always going to be running.

## Adding Users to Ghidra Server

1. Ensure client is using Ghidra version 10.0.4. Release can be found here: https://github.com/NationalSecurityAgency/ghidra/releases/tag/Ghidra_10.0.4_build
2. SSH into the Ghidra box. `$ ssh ubuntu@54.189.66.181`
3. Run the shell script present in the home directory. `$ ./user.sh <username>`
4. Default password is `changeme`,

## Adding Users Manually to Wireguard VPN

```
VPC -> 10.100.0.0/16
AWS HARDWARE -> 10.100.0.0/24
OSUSEC USERS -> 10.100.1.0/24 (In wireguard)
OSUSEC PHYSICAL SERVERS -> 10.100.2.0/24 (In wireguard)
```

The procedure for adding a user to the VPN is as follows:

1. SSH into VPN box `100.21.91.50`.
2. SU to root user.
3. Run `$ vim /etc/wireguard/wg0.conf` and add the following to the end of the file:

    ```
    [Peer]
    Endpoint = 0.0.0.0:13337
    AllowedIPs = 10.100.2.X/32
    PublicKey = PUB_KEY
    ```

    Where `X` is the previous IP incremented by one and `PUB_KEY` is the user's Wireguard public key.  

4. Restart the wg service with `$ sudo systemctl restart wg-quick@wg0`.  
5. Give the following config to user and have them replace `PRIVATE_KEY` with their wg private key and replace `X` with the corrosponding IP given in step `3`:

    ```
    [Interface]
    PrivateKey = PRIVATE_KEY
    ListenPort = 13337
    Address = 10.100.2.X/32
    DNS = 10.100.1.45

    [Peer]
    PublicKey = jIeRyIFz2yS/3lrn47IobKp5+DbWRpfYG+MfgRoKvWs=
    AllowedIPs = 10.100.1.0/24, 10.100.0.0/24, 10.100.20.0/24
    Endpoint = 100.21.91.50:13337
    ```
