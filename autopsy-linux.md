# Installing Autopsy

## Linux (Debian Based)

- Get the required files from the downloads page [here](https://www.autopsy.com/download/)

Run

```sh
sudo apt-get install testdisk
wget -q -O - https://download.bell-sw.com/pki/GPG-KEY-bellsoft | sudo apt-key add -
echo "deb [arch=amd64] https://apt.bell-sw.com/ stable main" | sudo tee /etc/apt/sources.list.d/bellsoft.list
sudo apt-get update
sudo apt-get install bellsoft-java8-full
export JAVA_HOME=/usr/lib/jvm/bellsoft-java8-full-amd64
```

Ensure that this java installation has worked with `java -version` and you should see:

```
    openjdk version "1.8.0.232"

    OpenJDK Runtime Environment (build 1.8.0_232-BellSoft-b10)
    
    OpenJDK 64-Bit Server VM (build 25.232-b10, mixed mode)
```

> Note: I got only a similar output from above, however not identical, and it worked fine.

Then, you will install the The Sleuth Kit bindings:

```sh
sudo apt install ./sleuthkit-java_4.8.0-1_amd64.deb
```

Then install autopsy:

- Extract the contents of the Autopsy ZIP file to a folder.
- Open a terminal and cd into the Autopsy folder.
- Make executable and run the `unix_setup.sh` script to configure Autopsy. You should see:

```
---------------------------------------------
Checking prerequisites and preparing Autopsy:
---------------------------------------------
Checking for PhotoRec...found in /usr/bin
Checking for Java...found in /usr/lib/jvm/bellsoft-java8-full-amd64
Checking for Sleuth Kit Java bindings...found in /usr/share/java
Copying sleuthkit-4.10.1.jar into the Autopsy directory...done

Autopsy is now configured. You can execute bin/autopsy to start it
```

And try out starting up autopsy by either navigating to the autopsy `bin/` directory or adding this directory to path, and running `./autopsy`. When this opens it will ask for hash collection, which happened to be behind the main autopsy image, so it will be hard to accept or decline this which will make it look like Autopsy is frozen.

For troubleshooting please read the official linux/OSX troubleshooting section [here](https://github.com/sleuthkit/autopsy/blob/develop/Running_Linux_OSX.txt#L104)
