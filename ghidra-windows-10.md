# Installing Ghidra

## Versions

- Windows 10 Pro 20H2, 19042.630
- Ghidra 9.2
- Java JDK 11.0.9

## Downloads

- Ghidra from <https://ghidra-sre.org/>
- Java 11 JDK from <https://www.oracle.com/java/technologies/javase-jdk11-downloads.html>


## Steps

1. Download Java from the link above. Sign in will be required.
2. Complete the Java installation wizard, install to the default location, presumed in this guide to be `C:\Program Files\Java\jdk-11.0.9`.
3. Download Ghidra
4. Unzip the downloaded Ghidra folder.
5. Set your `PATH` and `JAVA_HOME` variables as follows:
	- Navingate to **Control Panel** > **System and Security** > **System** > **Advanced system settings** > **Environment Variables...**, and make all changes under **System variables**.
	- Add `C:\Progra~1\Java\jdk-11.0.9\bin` to the `PATH` or `Path` variable. Make this the first entry in your path. Note that `Program Files` was converted to `Progra~1` - this removes the space, which apparently causes errors.
	- `JAVA_HOME` = `C:\Progra~1\Java\jdk-11.0.9`. Note there is no `\bin` here.
6. Navigate to the location you extracted Ghidra to, and double click `ghidraRun.bat` to start Ghidra up.

