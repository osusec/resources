# Building `john` from source

## Debian-Based Linux

It is possible to install just `john` without utilities such as `zip2john` using `apt` or other package manager. To make use of the full features of `john`, it's recommended to build it from source, which builds all the accessory tools.

```sh
# Install required and recommended software
sudo apt-get install -y build-essential libssl-dev yasm libgmp-dev libpcap-dev libnss3-dev libkrb5-dev pkg-config libbz2-dev zlib1g-dev git

# Make a src folder, and cd into it
mkdir ~/src
cd ~/src

# Clone john from github
git clonegit@github.com:openwall/john.git -b bleeding-jumbo john

# Navigate to john's src directory
cd john/src

# Configure and Build
./configure && make -s clean && make -sj4
```

Now `john` is built, and it's executables are located in `~/john/run/`. 

To test it, you may use `~/john/run/john --test`. 

The way that one typically uses `john` is to use one of the included utilities (i.e. `zip2john`) to extract the hash or encrypted password to a format `john` can brute force, then allow `john` to crack that file, providing it the `-format` flag. 

